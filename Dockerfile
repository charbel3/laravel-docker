FROM php:8.2.0alpha1-fpm-alpine

# Install ext-gd (image processing library)
RUN apk update \
    && apk upgrade \
    && apk add --no-cache build-base libpng libpng-dev libjpeg jpeg-dev zlib-dev freetype freetype-dev libzip-dev zip \
    && docker-php-ext-install exif \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

# Install some php extensions laravel depends on
RUN docker-php-ext-install pdo pdo_mysql zip

# Install some extra tools like git / node / npm ...
RUN apk add --update git curl

# install composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Install & Configure Nginx
RUN apk add --update nginx && rm -rf /var/cache/apk/*
RUN mkdir -p /tmp/nginx/client-body

COPY config/default.conf /etc/nginx/http.d/default.conf

# Specify the directory we are working in and navigate to it
WORKDIR /var/www/html/

# Copy Code into container
COPY ./ ./

# Install Dependencies
RUN composer install

# Create Symbolic link for laravel's storage folder
RUN php artisan storage:link

# Give right permissions for files & folders
RUN chown -R $USER:www-data storage
RUN chown -R $USER:www-data bootstrap/cache public/uploads
RUN chmod -R 775 bootstrap/cache storage public/uploads

RUN mkdir -p /run/nginx
RUN sed -i "s/user  nginx;/user  www-data;/g" /etc/nginx/nginx.conf
CMD php-fpm --daemonize; /usr/sbin/nginx -g 'daemon off;'